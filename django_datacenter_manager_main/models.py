from django_mdat_customer.django_mdat_customer.models import *


class DcInstance(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, db_column="customer")
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "dc_instance"
